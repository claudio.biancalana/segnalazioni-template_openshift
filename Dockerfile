FROM fedora

COPY storage.conf /etc/containers/

RUN dnf install -y \
  buildah \
  podman \
  make \
  && \
 dnf clean all

COPY anticorruzione.cer /etc/pki/ca-trust/source/anchors/
RUN update-ca-trust extract
